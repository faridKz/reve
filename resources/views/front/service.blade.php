<div class="container-fluid main-pad">
  <div class="title">
      <h3>Our Service Available at</h3>
  </div>

  <div class="content-service row">
    <div class="col-md-6 no-padding">
      <div class="col-md-12 padding-service">
        <div class="kotak-service1">
          <h3 class="bold">Jakarta</h3>
          <img src="/{{Config::get('path.images')}}/jakarta.jpg" alt="">
        </div>
      </div>
      <div class="col-md-6 padding-service">
        <div class="kotak-service2">
          <h3 class="bold">Bali</h3>
          <img src="/{{Config::get('path.images')}}/bali.jpg" alt="">
        </div>
      </div>
      <div class="col-md-6 no-padding">
        <div class="col-md-12 padding-service">
          <div class="kotak-service1">
            <h3 class="bold">Surabaya</h3>
            <img src="/{{Config::get('path.images')}}/surabaya.jpg" alt="">
          </div>
        </div>
        <div class="col-md-12 padding-service">
          <div class="kotak-service1">
            <h3 class="bold">Palembang</h3>
            <img src="/{{Config::get('path.images')}}/palembang.jpg" alt="">
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6 no-padding">
      <div class="col-md-6 no-padding">
        <div class="col-md-12 padding-service">
          <div class="kotak-service1">
            <h3 class="bold">Yogjakarta</h3>
            <img src="/{{Config::get('path.images')}}/yogja.jpg" alt="">
          </div>
        </div>
        <div class="col-md-12 padding-service">
          <div class="kotak-service1">
            <h3 class="bold">Solo</h3>
            <img src="/{{Config::get('path.images')}}/solo.jpg" alt="">
          </div>
        </div>
      </div>
      <div class="col-md-6 padding-service">
        <div class="kotak-service2">
          <h3 class="bold">Bandung</h3>
          <img src="/{{Config::get('path.images')}}/bandung.jpg" alt="">
        </div>
      </div>
      <div class="col-md-12 padding-service">
        <div class="kotak-service1">
          <h3 class="bold">Semarang</h3>
          <img src="/{{Config::get('path.images')}}/semarang.jpg" alt="">
        </div>
      </div>
    </div>
    {{-- <div class="col-md-6  ">
      <div class="row no-padding">
        <div class="padding-service">
          <div class="kotak-service1">
            <h3 class="bold">Jakarta</h3>
            <img src="/{{Config::get('path.images')}}/jakarta.jpg" alt="">

          </div>
        </div>
      </div>
      <div class="row no-padding">
        <div class="col-md-6 no-padding padding-service">
          <div class="kotak-service2">

          </div>
        </div>
        <div class="col-md-6 no-padding">
          <div class="padding-service">
            <div class="kotak-service1">

            </div>
          </div>
          <div class="padding-service">
            <div class="kotak-service1">

            </div>
          </div>
        </div>
      </div>
    </div> --}}
    {{-- <div class="col-md-6  ">
      <div class="row no-padding">
        <div class="col-md-6 no-padding">
          <div class="padding-service">
            <div class="kotak-service1">

            </div>
          </div>
          <div class="padding-service">
            <div class="kotak-service1">

            </div>
          </div>
        </div>
        <div class="col-md-6 no-padding padding-service">
          <div class="kotak-service2">

          </div>
        </div>
      </div>
      <div class="row no-padding">
        <div class="col-md-12 no-padding padding-service">
          <div class="kotak-service1">

          </div>
        </div>
      </div>
    </div> --}}
  </div>
</div>
