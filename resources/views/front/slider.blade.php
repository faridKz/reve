<div id="slider-carousel" class="carousel slide" data-ride="">
  {{-- indikator --}}
  <ol class="carousel-indicators">
    <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
    <li data-target="#slider-carousel" data-slide-to="1"></li>
    <li data-target="#slider-carousel" data-slide-to="2"></li>
    <li data-target="#slider-carousel" data-slide-to="3"></li>
    <li data-target="#slider-carousel" data-slide-to="4"></li>
  </ol>

  <div class="carousel-inner">
    <div class="item active">
      <div class="slider conference">
        <div class="middle">
          <h1 class="bold">Gabung Bersama Kami</h1>
          <h2>dan penuhi apa yang eventmu butuhkan</h2>
        <div class="margin-buttonIndex">
          <a href="#" class="button">Register</a>
        </div>
        <div class="margin-kategoriIndex">
          <h3>Conference</h3>
        </div>
        </div>
      </div>
    </div>

    <div class="item">
      <div class="slider conference">
        <div class="middle">
          <h1 class="bold">Gabung Bersama Kami</h1>
          <h2>dan penuhi apa yang eventmu butuhkan</h2>
        <div class="margin-buttonIndex">
          <a href="#" class="button">Register</a>
        </div>
        <div class="margin-kategoriIndex">
          <h3>Conference</h3>
        </div>
        </div>
      </div>
    </div>

    <div class="item">
      <div class="slider conference">
        <div class="middle">
          <h1 class="bold">Gabung Bersama Kami</h1>
          <h2>dan penuhi apa yang eventmu butuhkan</h2>
        <div class="margin-buttonIndex">
          <a href="#" class="button">Register</a>
        </div>
        <div class="margin-kategoriIndex">
          <h3>Conference</h3>
        </div>
        </div>
      </div>
    </div>

    <div class="item">
      <div class="slider conference">
        <div class="middle">
          <h1 class="bold">Gabung Bersama Kami</h1>
          <h2>dan penuhi apa yang eventmu butuhkan</h2>
        <div class="margin-buttonIndex">
          <a href="#" class="button">Register</a>
        </div>
        <div class="margin-kategoriIndex">
          <h3>Conference</h3>
        </div>
        </div>
      </div>
    </div>

    <div class="item">
      <div class="slider conference">
        <div class="middle">
          <h1 class="bold">Gabung Bersama Kami</h1>
          <h2>dan penuhi apa yang eventmu butuhkan</h2>
        <div class="margin-buttonIndex">
          <a href="#" class="button">Register</a>
        </div>
        <div class="margin-kategoriIndex">
          <h3>Conference</h3>
        </div>
        </div>
      </div>
    </div>

    <a class="left carousel-control" href="#slider-carousel" data-slide="prev"><img src="/{{Config::get('path.images')}}/arrow-left.png" alt=""></a>
    <a class="right carousel-control" href="#slider-carousel" data-slide="next"><img src="/{{Config::get('path.images')}}/arrow-right.png" alt=""></a>
  </div>
</div>
