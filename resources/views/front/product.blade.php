<div class="container-fluid main-pad slide-padding" id="product">
  <div class="title">
      <h3>Popular Product</h3>
  </div>
  <div class="col-md-12 no-padding goods">
    <div class="title-product row">
      <h4 class="bold">Goods for Rent</h4>
    </div>
    <div class="row">
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12 no-padding goods">
    <div class="title-product row">
      <h4 class="bold">Service</h4>
    </div>
    <div class="row">
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12 no-padding goods">
    <div class="title-product row">
      <h4 class="bold">Venue</h4>
    </div>
    <div class="row">
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
      <div class="col-md-2 padding-product">
        <div class="kotak-product">
          <div class="thumb-product">
            <img src="/{{Config::get('path.images')}}/pic-product.png" alt="">
          </div>
          <div class="content-product">
            <p class="bold">Camera</p>
            <p class="inline">Start form <p class="bold inline">Rp 50.000</p></p>
            <p class="inline">Mostly used on <p class="green-font inline">Conference, Photoshoot, Wedding</p></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
