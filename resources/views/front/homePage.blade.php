<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="/{{Config::get('path.css')}}/bootstrap.min.css">
    <link rel="stylesheet" href="/{{Config::get('path.css')}}/home.css">
  </head>
  <body>

    <nav class="navbar navbar-fixed-top myNavbar">
      <div class="container no-padding">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#targetNav">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img src="/{{Config::get('path.images')}}/logo.png" class="logo-header"></a>
        </div>
        <div class="collapse navbar-collapse" id="targetNav">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">How It Works</a></li>
            <li><a href="#">Sign Up</a></li>
            <li><a href="#">Log In</a></li>
          </ul>
        </div>
      </div>
    </nav>

    @include('front.slider')
    @include('front.works')
    @include('front.testi')
    @include('front.product')
    @include('front.service')

    <footer>
      <div class="row">
        <div class="col-md-12 no-padding">
          <div class="row">
            <div class="about pull-left">
              <a href="#" class="green-font bold">About</a>
              <a href="#" class="green-font bold">How It Works</a>
            </div>
            <div class="social pull-right">
              <a href="#"><img src="/{{Config::get('path.images')}}/line.png" alt=""></a>
              <a href="#"><img src="/{{Config::get('path.images')}}/insta.png" alt=""></a>
              <a href="#"><img src="/{{Config::get('path.images')}}/fb.png" alt=""></a>
              <a href="#"><img src="/{{Config::get('path.images')}}/twit.png" alt=""></a>
            </div>
          </div>
          <div class="copyright pull-right">
            <img src="/{{Config::get('path.images')}}/copyright.png" alt="">
          </div>
        </div>

      </div>
    </footer>
    <script src="/{{Config::get('path.js')}}/jquery.min.js"></script>
    <script src="/{{Config::get('path.js')}}/bootstrap.min.js"></script>

  </body>
</html>
