<div class="container-fluid no-padding" id="testi">
  <div class="title-testi">
      <h3>Lastest Event We Provided</h3>
  </div>
  <div id="testi-carousel" class="carousel slide" data-ride="">
    {{-- indikator --}}
    <ol class="carousel-indicators">
      <li data-target="#testi-carousel" data-slide-to="0" class="active"></li>
      <li data-target="#testi-carousel" data-slide-to="1"></li>
      <li data-target="#testi-carousel" data-slide-to="2"></li>
      <li data-target="#testi-carousel" data-slide-to="3"></li>
      <li data-target="#testi-carousel" data-slide-to="4"></li>
    </ol>

    <div class="carousel-inner">
      <div class="item active">
        <div class="col-md-12 no-padding">
          <div class="background">
            <img class="testi-back" src="/{{Config::get('path.images')}}/testi.jpg" alt="">
          </div>
          <div class="content-testi">
            <div class="col-md-7">
              <h4 class="bold">Android Workshop</h4>
              <h5>21 October 2017</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="col-md-offset-2 col-md-3">
              <ol class="items-table">
                <li>Items</li>
                <li>Camera</li>
                <li>Sound System</li>
                <li>T-Shirt Vendor</li>
                <li>Projector</li>
                <li>Venue</li>
              </ol>
            </div>
          </div>
        </div>
      </div>

      <div class="item">
        <div class="col-md-12 no-padding">
          <div class="background">
            <img class="testi-back" src="/{{Config::get('path.images')}}/testi.jpg" alt="">
          </div>
          <div class="content-testi">
            <div class="col-md-7">
              <h4 class="bold">Android Workshop</h4>
              <h5>21 October 2017</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="col-md-offset-2 col-md-3">
              <ol class="items-table">
                <li>Items</li>
                <li>Camera</li>
                <li>Sound System</li>
                <li>T-Shirt Vendor</li>
                <li>Projector</li>
                <li>Venue</li>
              </ol>
            </div>
          </div>
        </div>
      </div>

      <div class="item">
        <div class="col-md-12 no-padding">
          <div class="background">
            <img class="testi-back" src="/{{Config::get('path.images')}}/testi.jpg" alt="">
          </div>
          <div class="content-testi">
            <div class="col-md-7">
              <h4 class="bold">Android Workshop</h4>
              <h5>21 October 2017</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="col-md-offset-2 col-md-3">
              <ol class="items-table">
                <li>Items</li>
                <li>Camera</li>
                <li>Sound System</li>
                <li>T-Shirt Vendor</li>
                <li>Projector</li>
                <li>Venue</li>
              </ol>
            </div>
          </div>
        </div>
      </div>

      <div class="item">
        <div class="col-md-12 no-padding">
          <div class="background">
            <img class="testi-back" src="/{{Config::get('path.images')}}/testi.jpg" alt="">
          </div>
          <div class="content-testi">
            <div class="col-md-7">
              <h4 class="bold">Android Workshop</h4>
              <h5>21 October 2017</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="col-md-offset-2 col-md-3">
              <ol class="items-table">
                <li>Items</li>
                <li>Camera</li>
                <li>Sound System</li>
                <li>T-Shirt Vendor</li>
                <li>Projector</li>
                <li>Venue</li>
              </ol>
            </div>
          </div>
        </div>
      </div>

      <div class="item">
        <div class="col-md-12 no-padding">
          <div class="background">
            <img class="testi-back" src="/{{Config::get('path.images')}}/testi.jpg" alt="">
          </div>
          <div class="content-testi">
            <div class="col-md-7">
              <h4 class="bold">Android Workshop</h4>
              <h5>21 October 2017</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="col-md-offset-2 col-md-3">
              <ol class="items-table">
                <li>Items</li>
                <li>Camera</li>
                <li>Sound System</li>
                <li>T-Shirt Vendor</li>
                <li>Projector</li>
                <li>Venue</li>
              </ol>
            </div>
          </div>
        </div>
      </div>

      <a class="left carousel-control" href="#testi-carousel" data-slide="prev"><img src="/{{Config::get('path.images')}}/arrow-left.png" alt=""></a>
      <a class="right carousel-control" href="#testi-carousel" data-slide="next"><img src="/{{Config::get('path.images')}}/arrow-right.png" alt=""></a>
    </div>
  </div>

</div>
