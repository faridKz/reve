<div class="container-fluid main-pad slide-padding" id="works">
  <div class="title">
      <h3>How It Works</h3>
  </div>

  <ol class="works-list no-padding row">
    <li class="col-md-4">
      <div class="kotak-works">
        <div class="thumb-works">
          <img src="/{{Config::get('path.images')}}/work1.png" alt="">
        </div>
        <p>Buat event mu dan daftar semua kebutuhannya.</p>
      </div>
    </li>

    <li class="col-md-4">
      <div class="kotak-works">
        <div class="thumb-works">
          <img src="/{{Config::get('path.images')}}/work2.png" alt="">
        </div>
        <p>Pilih vendor terbaik untuk tiap item yang kamu butuhkan.</p>
      </div>
    </li>

    <li class="col-md-4">
      <div class="kotak-works">
        <div class="thumb-works">
          <img src="/{{Config::get('path.images')}}/work3.png" alt="">
        </div>
        <p>Pickup atau kitim barang sesuai keinginan. Kembalikan setelah event mu selesai.</p>
      </div>
    </li>
  </ol>

  <div class="button-position">
    <a href="#" class="button">Learn More</a>
  </div>


  </div>
</div>
